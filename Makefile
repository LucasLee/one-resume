#!/usr/bin/make -f

STACK_ENVIRONMENT ?= ${environment}
stack_name = one-resume-${STACK_ENVIRONMENT}
cf_input_template = ./dist/template.yaml
cf_output_template = ./dist/transformed.yaml
aws_region = us-east-1
stack_description = 'One resume to rule them all'
stack_environment_exists = ${STACK_ENVIRONMENT}

ifndef aws_command
	aws_command := $(shell command -v aws 2> /dev/null)
endif
aws_s3_bucket_exists := $(shell aws s3api list-buckets --query "Buckets[?Name=='${AWS_S3_BUCKET_UPLOAD}']" --output text)
done = @echo ✅ $@ done

help: ## Display this help
	@echo "\nUsage:\n  make \033[36m<target> \033[33m[options] \033[0m\n\nTargets:\n"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2}'
	@echo "\nOptions:\n  \033[33menvironment=<env> \033[0m Update a specific environment e.g dev,sandbox,production\n"

.PHONY: pre-build
pre-build: clean
pre-build:
	# Requirements
	pip install -r ./requirements/deploy.txt
	
	mkdir -p dist

	# Merge the cloudformation templates
	python3 ./cloudformation/merger.py 
	$(DONE)
 
.PHONY: build
build: pre-build
build: 
	npm install ;
	npm run build
	$(DONE)

.PHONY: deploy
deploy: check-arguments-and-dependencies check-aws-s3-bucket
deploy: build
	sam deploy \
	--stack-name $(stack_name) \
	--capabilities CAPABILITY_IAM \
	--capabilities CAPABILITY_NAMED_IAM \
	--region $(aws_region) \
	--no-fail-on-empty-changeset \
	--tags Environment=${STACK_ENVIRONMENT} \
	--parameter-overrides \
	route53ZoneName=${ROUTE_53_ZONE_NAME} \
	customHostname=${CUSTOM_HOSTNAME} \
	httpsCertificateArn=${HTTPS_CERTIFICATE_ARN} \
	apiOriginHostname=${API_ORIGIN_HOSTNAME} \
	stackDescription=${stack_description}
	$(done)
 
.PHONY: upload
upload: 
	$(aws_command) s3 cp --cache-control no-cache --expires 0 build s3://$(BUCKET_NAME)/ --recursive
	$(done)

.PHONY: clean
clean:
	rm -rf dist/
	$(done)

.PHONY: check-arguments-and-dependencies
check-arguments-and-dependencies:
ifndef stack_environment_exists
	$(error Please enter the name of the stack (i.e. stack_environment=dev))
endif

ifndef aws_command
	$(error AWS CLI is not available. Please install AWS CLI via `pip install awscli boto3`.)
endif

.PHONY: check-aws-s3-bucket
check-aws-s3-bucket:
ifndef aws_s3_bucket_exists
	$(error Bucket does not exist within the specified account. Please specify a proper bucket as an option (i.e. BUCKET_NAME=my-bucket))
endif