# Check if a env variable is passed as the first argument
if [ -z "$1" ]
then
    echo "Pass in env_name as first argument"
    echo "=================================="
    echo "Syntax  : ./local_deploy.sh <env_name>"
    echo "Example : ./local_deploy.sh dev"
else
    echo Deploying to env_name: $1
    export HTTPS_CERTIFICATE_ARN=arn:aws:acm:us-east-1:953652993425:certificate/57329bb3-555f-40cc-afe9-41b1962a53e2
    export ROUTE_53_ZONE_NAME=lucaslee.net
    export CI_ENVIRONMENT_NAME=$1
    export AWS_S3_BUCKET_UPLOAD=cloudformation-deployer
    export CUSTOM_HOSTNAME=lucas
    export API_ORIGIN_HOSTNAME=yahoo.com
    export BUCKET_NAME=one-resume-develop-s3-bucket

    if command -v python3.8  &> /dev/null
    then
        python3.8 -m venv venv/38
        . ./venv/38/bin/activate
        make deploy environment=${CI_ENVIRONMENT_NAME}
        make upload
    elif command -v python3  &> /dev/null
    then
        python3 -m venv venv/3
        . ./venv/3/bin/activate
        make deploy environment=${CI_ENVIRONMENT_NAME}
        make upload
    else
        echo "You don't have python3 or python3.8 installed"
    fi
fi