
  /**
   * Replace the content of this file (/src/manifest.ts) with the manifest data copied by the export button
   */
   import ManifestModel from './models/ManifestModel';

   export const manifest:ManifestModel = {
     "intro": {
         "name": "Lucas Lee",
         "position": "Front-End Engineer",
         "statement": "Passionate about Javascript, specialized in rapid development with a strong sense of design. \nExperienced with a wide range of skillset that extends from multiple frontend frameworks to containerized Devops. Frontend Tech Lead for EAB Enrollment Service. "
     },
     "skills": [
         {
             "icon": "Js",
             "id": "2f5477ec-8023-4137-a67c-2e731ec87ffd",
             "interest": "Very High",
             "lastUsed": "Recently",
             "level": 9,
             "name": "Javascript"
         },
         {
             "icon": "Js",
             "id": "53274b17-295e-45e6-bf96-54ca6ff195d0",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 8,
             "name": "Typescript"
         },
         {
             "icon": "React",
             "id": "778b06b9-c65f-48b3-94fd-caa94586f2a2",
             "interest": "Very High",
             "lastUsed": "Recently",
             "level": 8.5,
             "name": "React"
         },
         {
             "icon": "Node",
             "id": "8d815e92-8425-4533-b22a-d18e1eaa3d15",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 8,
             "name": "Node.js"
         },
         {
             "icon": "Python",
             "id": "949056fc-dd63-4fec-a17d-893fff91324a",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 7,
             "name": "Python"
         },
         {
             "icon": "Angular",
             "id": "3616213c-314e-4b54-a0d6-b624f6c9519f",
             "interest": "High",
             "lastUsed": "1 Year+ Ago",
             "level": 7,
             "name": "Angular"
         },
         {
             "icon": "Angular",
             "id": "ddd39fa0-14f7-43f8-b261-b2dd6ceb63af",
             "interest": "Low",
             "lastUsed": "1 Year+ Ago",
             "level": 6,
             "name": "AngularJS"
         },
         {
             "name": "Docker",
             "level": 7,
             "interest": "High",
             "icon": "Docker",
             "lastUsed": "Recently",
             "id": "5d4d0063-0793-4741-88d2-85e5266172b8"
         },
         {
             "icon": "React",
             "id": "4e197daa-9f50-4b29-a4a1-e89a9d791538",
             "interest": "Very High",
             "lastUsed": "1 Year+ Ago",
             "level": 6,
             "name": "React Native"
         },
         {
             "icon": "Dev",
             "id": "ec069038-7c8e-4569-ad68-b2c3e9ea34fa",
             "name": "MongoDB",
             "level": 5,
             "interest": "Moderate",
             "lastUsed": "This Year"
         },
         {
             "name": "Java",
             "level": 4,
             "interest": "Moderate",
             "icon": "Java",
             "lastUsed": "1 Year+ Ago",
             "id": "8aec79f7-3f01-4787-852b-97f8429d2290"
         },
         {
             "name": "Photoshop",
             "level": 7,
             "interest": "Moderate",
             "icon": "Adobe",
             "lastUsed": "This Month",
             "id": "9869bf78-1c6e-44f7-8413-91a5ca0a0ee1"
         },
         {
             "icon": "Js",
             "id": "2d000364-12f9-4c9f-abf1-79d0e5d88584",
             "interest": "Low",
             "lastUsed": "This Year",
             "level": 7,
             "name": "JQuery"
         },
         {
             "icon": "Adobe",
             "id": "b50f9291-c417-42a0-ab24-78cb2cb510c2",
             "interest": "Low",
             "lastUsed": "This Year",
             "level": 7,
             "name": "Coldfusion"
         },
         {
             "icon": "Sass",
             "id": "f0d290bd-9383-4395-8785-00db27fa47e0",
             "name": "CSS/SCSS",
             "level": 7,
             "interest": "High",
             "lastUsed": "Recently"
         },
         {
             "icon": "Aws",
             "id": "feb0243c-ba87-42f2-bc25-456f19a349d6",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 8,
             "name": "Step Function"
         },
         {
             "icon": "Aws",
             "id": "fc5763ad-e92c-43c6-96bb-9694a355186a",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 8,
             "name": "Lambda"
         },
         {
             "icon": "Aws",
             "id": "025a36f8-c1e5-41b4-898c-9105128b7360",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 7,
             "name": "Cloudformation"
         },
         {
             "icon": "Aws",
             "id": "2f9ae1cf-8347-4450-9cd3-fd314f9b5859",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 7,
             "name": "Dynamodb"
         },
         {
             "icon": "Aws",
             "id": "aee906f1-8527-464f-ae83-c5a6676d2d6d",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 7,
             "name": "S3"
         },
         {
             "icon": "Aws",
             "id": "d494c2cf-1850-499f-bad8-8aadeaa52b06",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 7,
             "name": "Cloudfront"
         },
         {
             "icon": "Aws",
             "id": "b41ad8bf-2ebc-486c-bcc0-5a1ecf769fc5",
             "interest": "High",
             "lastUsed": "Recently",
             "level": 6,
             "name": "RDS"
         }
     ],
     "experiences": [
         {
             "bulletPoints": [
                 {
                     "point": "Team Lead for development of a frontend portal for serving analytical data for multi tenancy clients.",
                     "skills": [
                         "Cloudformation",
                         "Cloudfront",
                         "Javascript",
                         "Lambda",
                         "Python",
                         "RDS",
                         "React",
                         "S3",
                         "Typescript"
                     ]
                 },
                 {
                     "point": "Team Lead for development of a replication service for pulling large scale operational and analytical data using serverless technologies.",
                     "skills": [
                         "Cloudformation",
                         "Dynamodb",
                         "Lambda",
                         "Python",
                         "Step Function"
                     ]
                 },
                 {
                     "point": "Frontend Tech Lead for creating a document routing tool that generates batched pdf for partners.",
                     "skills": [
                         "Cloudformation",
                         "Cloudfront",
                         "Javascript",
                         "Lambda",
                         "Python",
                         "RDS",
                         "React",
                         "S3"
                     ]
                 },
                 {
                     "point": "Frontend Tech Lead for development of a multi-tenant data management tool for managing partner data.",
                     "skills": [
                         "Cloudformation",
                         "Cloudfront",
                         "Javascript",
                         "Lambda",
                         "Python",
                         "RDS",
                         "React",
                         "S3"
                     ]
                 },
                 {
                     "point": "Frontend Tech Lead for researching and POCing a new Marketing Platform using AWS technologies.",
                     "skills": [
                         "Cloudformation",
                         "Cloudfront",
                         "Javascript",
                         "Lambda",
                         "Python",
                         "React",
                         "S3"
                     ]
                 }
             ],
             "company": "EAB Enrollment Service",
             "date": "SEPT 2019 - CURRENT",
             "id": "a985983a-792b-4121-bb4e-c226b96d5873",
             "position": "Front-End Engineer"
         },
         {
             "bulletPoints": [
                 {
                     "point": "Frontend Tech Lead for developing a Stripe payment system to handle live student application payments.",
                     "skills": [
                         "CSS/SCSS",
                         "Coldfusion",
                         "JQuery",
                         "Javascript"
                     ]
                 },
                 {
                     "point": "Created a responsive ad tools for generating google ad previews for routing to clients.",
                     "skills": [
                         "Angular",
                         "CSS/SCSS",
                         "Docker",
                         "Javascript",
                         "Typescript"
                     ]
                 },
                 {
                     "point": "Collaborate on the project to create a budget management tool for calculating estimated cost of display ad for business clients.",
                     "skills": [
                         "CSS/SCSS",
                         "Docker",
                         "Javascript",
                         "MongoDB",
                         "Node.js",
                         "React"
                     ]
                 },
                 {
                     "point": "Frontend Tech Lead for creating a platform for sending out SMS to large volume of Students.",
                     "skills": [
                         "CSS/SCSS",
                         "Coldfusion",
                         "JQuery",
                         "Javascript"
                     ]
                 },
                 {
                     "point": "Integrated Google Drive upload to Student Admission Application for submitting documents.",
                     "skills": [
                         "CSS/SCSS",
                         "Coldfusion",
                         "JQuery",
                         "Javascript"
                     ]
                 },
                 {
                     "point": "Developed a passwordless login system for Student Application.",
                     "skills": [
                         "CSS/SCSS",
                         "Coldfusion",
                         "JQuery",
                         "Javascript"
                     ]
                 },
                 {
                     "point": "Created a custom code review tool for SVN.",
                     "skills": [
                         "CSS/SCSS",
                         "Coldfusion",
                         "JQuery",
                         "Javascript",
                         "MongoDB"
                     ]
                 }
             ],
             "company": "EAB Enrollment Service",
             "date": "MAY 2015 - SEPT 2019",
             "id": "14694e38-f591-4384-a0dc-a3fc14da60bb",
             "position": "Senior Web Developer"
         },
         {
             "bulletPoints": [
                 {
                     "point": "Built a AngularJS touch based Point of Sales system that has been in used to serve customer for over 6 years.",
                     "skills": [
                         "AngularJS",
                         "Javascript",
                         "MongoDB",
                         "Node.js",
                         "Photoshop"
                     ]
                 }
             ],
             "company": "Point of Sales System for UMI Sushi Bistro, Short Pump",
             "date": "NOV 2014 - MAY 2015",
             "id": "cd3c3a53-a31d-4ea6-846a-b5299a5e58fb",
             "position": "Freelance"
         },
         {
             "bulletPoints": [
                 {
                     "point": "Built mobile application using a Javascript Platform (Appcelerator).",
                     "skills": [
                         "Javascript",
                         "Node.js",
                         "Photoshop"
                     ]
                 },
                 {
                     "point": "Built Full Stack Web Application with node.",
                     "skills": [
                         "CSS/SCSS",
                         "Javascript",
                         "MongoDB",
                         "Node.js",
                         "Photoshop"
                     ]
                 }
             ],
             "company": "Shockoe | Mobile by Design",
             "date": "MAR 2014 - NOV 2014",
             "id": "f8f52374-8c5b-4641-b306-3324c6111059",
             "position": "Mobile Application Developer"
         },
         {
             "id": "0ad46344-9029-42e3-a5aa-f89bca37e920",
             "company": "Virginia Commonwealth University",
             "date": "2009 - 2013",
             "position": "Computer Science, Bachelor's Degree",
             "bulletPoints": []
         }
     ]
 }