export default class SkillModel {
    public name: string = "";
    public level: number = 5;
    public interest: "Low"|"Moderate"|"High"|"Very High"| string = "High";
    public lastUsed: "1 Year+ Ago"|"This Year"|"Last Month"|"This Month"|"Recently" | string = "Current";
    public icon: string = "Js";
    public id?: string = "new";
}
