// React
import React, { ChangeEvent, useContext, useState } from "react";

// Context
import { types } from "../../context/reducers";
import { StoreContext } from "../../context/StoreContext";

// Material UI
import { Fab, TextField } from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";

// Model
import IntroModel from "../../models/IntroModel";

// Style
import "./Intro.component.scss";

// tslint:disable-next-line:no-empty-interface
export interface IIntroComponentProps {}

const IntroComponent = ( props: IIntroComponentProps ) => {
    const { state, dispatch } = useContext(StoreContext);
    const { position, name, statement }: IntroModel = state.intro;
    const editMode: boolean = state.editMode;
    const statementEditMode: boolean = state.statementEditMode;
    const positionEditMode: boolean = state.positionEditMode;
    const nameEditMode: boolean = state.nameEditMode;

    const toggleStatementEditMode = ( mode:boolean = !statementEditMode ) =>{ 
        dispatch( { 
            type: types.TOGGLE_STATEMENT_EDIT_MODE,
            mode,
        } );
    };


    const toggleNameEditMode = ( mode:boolean = !nameEditMode ) =>{ 
        dispatch( { 
            type: types.TOGGLE_NAME_EDIT_MODE,
            mode,
        } );
    };


    const togglePositionEditMode = ( mode:boolean = !positionEditMode ) =>{ 
        dispatch( { 
            type: types.TOGGLE_POSITION_EDIT_MODE,
            mode,
        } );
    };

    // local state
    const [ currentStatement, updateStatement ] = useState( statement );
    const [ currentPosition, updatePosition ] = useState( position );
    const [ currentName, updateName ] = useState( name );

    /**
     * If editMode is off, ensure local edit state are turned off
     */
    // useEffect( () => {
    //     if ( !editMode ) {
    //         toggleStatementEditMode( false );
    //         togglePositionEditMode( false );
    //         toggleNameEditMode( false );
    //     }
    // }, [ editMode ]);

    /**
     * Statement Input Update
     *
     * @param {ChangeEvent<HTMLInputElement>} event
     */
    const handleStatementChange = ( currentStatement: string ) => {
        dispatch(
            {
                type: types.UPDATE_STATEMENT,
                statement: currentStatement,
            },
        );
    };


    /**
     * Position Input Update
     *
     * @param {ChangeEvent<HTMLInputElement>} event
     */
    const handlePositionChange = ( currentPosition: string ) => {
        dispatch(
            {
              position: currentPosition,
              type: types.UPDATE_POSITION,
            },
        );
    };


    /**
     * Name Input Update
     * @param {ChangeEvent<HTMLInputElement>} event
     */
         const handleNameChange = ( currentName: string ) => {
            dispatch(
                {
                  name: currentName,
                  type: types.UPDATE_NAME,
                },
            );
        };


    const handleUpdate = ( variable: string ) => ( event: ChangeEvent<HTMLInputElement> ) => {
        switch (variable){ 
            case 'position':
                updatePosition( event.target.value );
                break;
            case 'statement':
                updateStatement( event.target.value );
                break;
            case 'name':
                updateName( event.target.value );
                break;
        }
    };


    return (
        <div className="intro">
            {/* Name
                --------
            */}
            <div className={"name" + (nameEditMode && editMode ? " extra-margin" : "") + (editMode && !nameEditMode ? " edit-outline" : "")}>
                {
                    /*  Check Button
                        ------------
                    */
                    nameEditMode && editMode ?
                    <Fab className="edit-icon" color="secondary" aria-label="Edit" onClick={() => handleNameChange(currentName)} >
                        <CheckIcon/>
                    </Fab> : undefined
                }
                { nameEditMode && editMode ?
                    /*  Name Input
                        ---------------
                    */
                    <TextField
                        label="Edit Name"
                        className="name-input"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleUpdate('name')}
                        value={currentName}
                    /> :
                    /*  Name View Mode
                        ------------------
                    */
                    <div onClick={() => editMode && toggleNameEditMode( !nameEditMode )}>
                        <h1 className="name">{name}</h1>
                    </div>
                }
            </div>
            {/* Position
                --------
            */}
            <div className={"position" + (positionEditMode && editMode ? " extra-margin" : "") + (editMode && !positionEditMode ? " edit-outline" : "")}>
                {
                    /*  Check Button
                        ------------
                    */
                    positionEditMode && editMode ?
                    <Fab className="edit-icon" color="secondary" aria-label="Edit" onClick={() => handlePositionChange(currentPosition)} >
                        <CheckIcon/>
                    </Fab> : undefined
                }
                { positionEditMode && editMode ?
                    /*  Position Input
                        ---------------
                    */
                    <TextField
                        label="Edit Position"
                        className="position-input"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleUpdate('position')}
                        value={currentPosition}
                    /> :
                    /*  Position View Mode
                        ------------------
                    */
                    <div onClick={() => editMode && togglePositionEditMode( !positionEditMode )}>
                        {position}
                    </div>
                }
            </div>
            {/* Statement
                --------
            */}
            <div className={"statement" + (statementEditMode && editMode ? " extra-margin" : "") + (editMode && !statementEditMode ? " edit-outline" : "")}>
                {
                    /*  Check Button
                        ------------
                    */
                    statementEditMode && editMode ?
                    <Fab className="edit-icon" aria-label="Edit" onClick={() => handleStatementChange(currentStatement)} >
                        <CheckIcon/>
                    </Fab> : undefined
                }
                { statementEditMode && editMode ?
                    /*  Statement Input
                        ---------------
                    */
                    <TextField
                        label="Edit Statement"
                        fullWidth={true}
                        multiline={true}
                        className="statement-input"
                        margin="normal"
                        style={{ margin: 8 }}
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleUpdate('statement')}
                        value={currentStatement}
                    /> :
                    /*  Statement View Mode
                        -------------------
                    */
                    <div onClick={() => editMode && toggleStatementEditMode( !statementEditMode )}>
                        {statement}
                    </div>
                }
            </div>
        </div>
    );
};

export default IntroComponent;
