import ruamel.yaml
from os import walk
 
yaml = ruamel.yaml.YAML()
 
RESOURCES_FOLDER = './cloudformation/resources'

BASE_CF_FILE = './cloudformation/base.yaml'

OUTPUT_FILE = './template.yaml'
 
TOP_LEVEL_KEYS = [ 
    'Parameters',
    'Outputs',
    'Resources'
]

with open(BASE_CF_FILE) as file_path:
    cloudformation_base = yaml.load(file_path)

for (dirpath, dirname, filenames) in walk(RESOURCES_FOLDER):
    for file in filenames: 
        file_path = dirpath + '/' + file
        with open(file_path, 'r') as fp:
            current_resource = yaml.load(fp)
            for top_level_key in TOP_LEVEL_KEYS:
                if top_level_key in current_resource:
                    for resource_key in  current_resource[top_level_key]:
                        if top_level_key not in cloudformation_base:
                            cloudformation_base[top_level_key] = {resource_key:current_resource[top_level_key][resource_key]}
                        else:
                            cloudformation_base[top_level_key].update({resource_key:current_resource[top_level_key][resource_key]})
 
yaml.dump(cloudformation_base, open(OUTPUT_FILE, 'w'))